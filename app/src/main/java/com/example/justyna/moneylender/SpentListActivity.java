package com.example.justyna.moneylender;

import android.app.ListActivity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;


public class SpentListActivity extends ListActivity {

//    ListView list;
    private Context ctx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        list = (ListView) findViewById(R.id.list);

        SpentDBHelper db = new SpentDBHelper(ctx);
        Cursor cr = db.getAllSpents(db);
        String[] fromFieldNames = new String[] {db.KEY_ID, db.KEY_PRICE, db.KEY_DESCRIPTION};
        int[] toViewIDs = new int[] {R.id.id_item, R.id.price_item, R.id.description_item};
        SimpleCursorAdapter myCursorAdapter = new SimpleCursorAdapter(ctx,R.layout.spent_item_layout, cr, fromFieldNames, toViewIDs,0);
        ListView mylist = (ListView) findViewById(R.id.list_item);
        mylist.setAdapter(myCursorAdapter);

    }
}
