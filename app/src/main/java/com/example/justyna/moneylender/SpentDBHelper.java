package com.example.justyna.moneylender;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SpentDBHelper extends SQLiteOpenHelper {

    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "spent.db";
    private static final String DB_SPENT_TABLE = "spent";

    public static final String KEY_ID = "_id";
    public static final String ID_OPTIONS = "INTEGER PRIMARY KEY AUTOINCREMENT";
    public static final int ID_COLUMN = 0;
    public static final String KEY_PRICE = "price";
    public static final String PRICE_OPTIONS = "INTEGER DEFAULT 0";
    public static final int PRICE_COLUMN = 1;
    public static final String KEY_DESCRIPTION = "description";
    public static final String DESCRIPTION_OPTIONS = "TEXT NOT NULL";
    public static final int DESCRIPTION_COLUMN = 2;

    private static final String DB_CREATE_SPENT_TABLE =
            "CREATE TABLE " + DB_SPENT_TABLE + "( " +
                    KEY_ID + " " + ID_OPTIONS + ", " +
                    KEY_PRICE + " " + PRICE_OPTIONS + ", " +
                    KEY_DESCRIPTION + " " + DESCRIPTION_OPTIONS +
                    ");";
    private static final String DROP_SPENT_TABLE =
            "DROP TABLE IF EXISTS " + DB_SPENT_TABLE;

    private static final String DEBUG_TAG = "Description of log";

    public SpentDBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DB_CREATE_SPENT_TABLE);

            Log.d(DEBUG_TAG, "Database creating...");
            Log.d(DEBUG_TAG, "Table " + DB_SPENT_TABLE + " ver." + DB_VERSION + " created");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(DROP_SPENT_TABLE);

            Log.d(DEBUG_TAG, "Database updating...");
            Log.d(DEBUG_TAG, "Table " + DB_SPENT_TABLE + " updated from ver." + oldVersion + " to ver." + newVersion);
            Log.d(DEBUG_TAG, "All data is lost.");

            onCreate(db);
        }

    public void insertRecord(SpentDBHelper db, String price, String description) {
        SQLiteDatabase sq = db.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_PRICE, price);
        values.put(KEY_DESCRIPTION, description);
        sq.insert(DB_SPENT_TABLE, null, values);
        sq.close();
    }

    public Cursor getAllSpents(SpentDBHelper db) {
        SQLiteDatabase sq = db.getWritableDatabase();
        String[] columns = {KEY_ID, KEY_PRICE, KEY_DESCRIPTION};
        return sq.query(DB_SPENT_TABLE, columns, null, null, null, null, null);
    }

}