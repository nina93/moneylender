package com.example.justyna.moneylender;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class LendActivity extends ActionBarActivity {

    private EditText amount;
    private EditText name;
    private EditText surname;
    private EditText phone_number;
    private EditText email;
    private Button save_lend;
    private Button button_show_history_lend;
    private Button button_main_menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lend);

        amount = (EditText) findViewById(R.id.amount);
        name = (EditText) findViewById(R.id.name);
        surname = (EditText) findViewById(R.id.surname);
        phone_number = (EditText) findViewById(R.id.phone_number);
        email = (EditText) findViewById(R.id.email);
        save_lend = (Button) findViewById(R.id.save_lend);
        button_show_history_lend = (Button) findViewById(R.id.button_show_history_lend);
        button_main_menu = (Button) findViewById(R.id.button_main_menu);

        save_lend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strAmount = amount.getText().toString();
                String strName = name.getText().toString();
                String strSurname = surname.getText().toString();
                String strPhoneNumber = phone_number.getText().toString();
                String strEmail = email.getText().toString();

                if(strAmount.isEmpty() || strName.isEmpty() || strSurname.isEmpty()) {
                    Toast.makeText(getBaseContext(), "Amount/Name/Surname can't be empty", Toast.LENGTH_LONG).show();
                }
                else {
//                    LendDBHelper db = new LendDBHelper(ctx);
//                    db.insertRecord(db, strAmount, strName, strSurname, strPhoneNumber, strEmail);
//                    Toast.makeText(getBaseContext(), "Saved", Toast.LENGTH_LONG).show();
                }
            }
        });

        button_show_history_lend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LendActivity.this, LendListActivity.class);
                startActivity(intent);
            }
        });

        button_main_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LendActivity.this, SpentListActivity.class);
                startActivity(intent);
            }
        });


    }
}
