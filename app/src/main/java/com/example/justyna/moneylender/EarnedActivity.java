package com.example.justyna.moneylender;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class EarnedActivity extends ActionBarActivity {

    private EditText price;
    private EditText description;
    private Button save;
    private Button history;
    private Button menu;
    private Context ctx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earned);

        price = (EditText) findViewById(R.id.price);
        description = (EditText) findViewById(R.id.description);
        save = (Button) findViewById(R.id.save_spent);
        history = (Button) findViewById(R.id.button_show_history);
        menu = (Button) findViewById(R.id.button_main_menu);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String strPrice = price.getText().toString();
                String strDescription = description.getText().toString();

                if(strDescription.isEmpty()) {
                    Toast.makeText(getBaseContext(), "Description can't be empty", Toast.LENGTH_LONG).show();
                }
                else {
//                    EarnedDBHelper db = new EarnedDBHelper(ctx);
//                    db.insertRecord(db, strPrice, strDescription);
//                    Toast.makeText(getBaseContext(), "Saved", Toast.LENGTH_LONG).show();
                }

            }
        });

        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(EarnedActivity.this, EarnedListActivity.class);
                startActivity(intent);

            }
        });


        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EarnedActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }



}
